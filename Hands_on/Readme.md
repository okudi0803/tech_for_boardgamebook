# 記者ハンドブックベース校正を自動化しよう



## 全体像

![gitlabCI](/uploads/16d0b2c5ceb29c2a16c371083984d957/gitlabCI.jpg)

要は「最初に設定ファイルを作るだけであとは自動で原稿の校正をかけてくれる」仕組み



## 手順

1. YahooAPIの開設(無料)
   https://e.developer.yahoo.co.jp/dashboard/
   から、「クライアントサイド」アプリケーションとしてAPIキーを発行する。

2. 「.textlintrc」を作成。

   ```json
   {
     "rules": {
    	// yahoo文章校正APIに接続するTextlintルールを利用する(appIDを下記のように設定する) 
       "ja-yahoo-kousei": {
         "appID": "(yahooアプリケーションID)"
       },
       // Web+DBの校正規則を適用するTextlintルールを利用する(特にパラメータはないのでTrueのみ)
       "web-plus-db": true
     }
   }
   
   ```

   ※JSONはコメントアウトができないので、コピペしたときはコメントを削除すること

   

3. 「.gitlab-ci.yml」を作成

   ```yaml
   # stage(テストフェーズ)構造を設定する
   stages:
     - lint
   
   # 「markdown_lint」というテスト内容を設定する
   markdown_lint:
   # 利用するDockerコンテナ名
     image: node:11.12.0-alpine
   # どのstageでこのテスト内容を実行するか
     stage: lint
   # 以下実行内容「before_script」「script」「after_script」に3分割して手順を設定できる
     before_script:
   # ライブラリのインストール。
   # 「textlint」本体と「textlint-rule-web-plus-db」「textlint-rule-ja-yahoo-kousei」というルールをインストールする
       - npm install -g textlint textlint-rule-web-plus-db textlint-rule-ja-yahoo-kousei
     script:
   # 「Textlint_Customize」フォルダ直下の「Readme.md」ファイルに校正をかける
       - textlint Textlint_Customize/Readme.md
   
   ```

4. 上記の内容をGitlabリポジトリにPushする



## 結果画面

![kousei_result](/uploads/3f8d388f30b1b85d1075d964e7de479e/kousei_result.jpg))



## セキュリティ(上級編)

やだ…APIキーべた書き怖い…という人もきちんと方法があります。

* gitlab CI variableの話
* 難しいようであればプライベートリポジトリにしてしまいましょう