# Textlint自動校正のカスタマイズ(上級編)

## 背景

* 複数人で執筆しており、表記が乱れやすい
* VSCode Snippetは整備したが、おそらくまだ乱れる可能性がある(VSCodeを使って執筆していない人がいた)
* 個人の努力より仕組み化
* 出来れば記者ハンドブック等の常用表記統一もやっていきたい
* ただし、今回は出来合いのルールではなくカスタマイズをしたかったので、配布が難しかった

## 全体像

![gitlab全体像](/uploads/86d4c62144b2102d55c29749d91bc7e2/gitlabci全体像2.jpg)

## 技術の概要

### 実装関連

* Textlint
  * Javascript(npm)ライブラリ
  * 静的コード解析(VSCodeで書いてると波線で注意してくれるアレ)である「ESlint」の仕組みを流用して、日本語をはじめとする文章校正ができるようになるパッケージ
  * いろいろな校正ルールがライブラリとして落ちており、コードを書かなくてもいろいろな校正ができる
    * 便利なルール集まとめ
      * https://efcl.info/2016/07/13/textlint-rule-preset-ja-technical-writing/
      * https://github.com/textlint/textlint/wiki/Collection-of-textlint-rule

### 配布関連

* Docker

  * どこかで名前は聞いたことあるはず

  * コンテナ仮想化を用いたアプリケーション実行・配置・実行ができるソフトウェア

  * 乱暴に言えば「アプリ実行に必要な部分だけまとめた軽量版OSを乗せた仮想サーバ」

    * VirtualBox、Vagrantよりも持ち運びや配布が簡単
    * その代わり最初に決めたOS機能しかもっていないため、用途や仕様を切ってからでないと「やりたいことができない」可能性が高い
    * ただ、それでも大体のことはできるので最近の仮想化のデファクトになりつつある

  * DockerHubという「既製品コンテナ配布サービス」があり、「誰かがいい感じに作った仮想環境」をダウンロードして環境構築をサボることができる

  * もちろん、自分でコンテナ作ることもできる(あまり作ることはないけど)

  * 【運用面】Dockerfileという1つのファイルに構成管理データ(OSの設定とか初期情報)を格納するため、ブラックボックス化しづらい

    

* Gitlab CI/CD

  * Gitlabが提供する自動テスト/デプロイプラットフォーム(CI/CD)

  * Gitlabリポジトリを開設したらだれでも自由に利用可能(無料！)

  * .gitlab-ci.ymlというファイルを作るだけで、Gitlabレポジトリのコードに対して任意のコマンドを実行してくれる

    * テストができる
    * サーバへの機能公開(デプロイ)等ができ、手作業でぽちぽちやらなくてもサービスの更新作業ができる(昔はファイルを移行してビルドして…が必要だった)

  * GitlabにPushされるたびにGitlabレポジトリに対して実行される

  * 今回はこれでDockerコンテナも自動生成した

    

* Gitlab Container registry

  * Gitlab CI/CDで作ったDockerコンテナを保存しておいてくれるクラウド空間

  * 各Gitlabレポジトリにこれもついてくる(無料！)

  * 過去に作った全コンテナについてはバージョン管理してGUIで閲覧可能にしてくれる

  * 作成したコンテナを使いたいときは

    ```bash
    docker run registry.gitlab.com/ユーザ名/レポジトリ名
    ```

    で使えるので、配布が簡単

  * 特に多人数で使う場合、パッケージとして公開することが必要になったりするが、

    * Gitlabはコード情報は残るが、パッケージとして利用可能にするためには自分でビルド/インストール作業が必要
    * npmやpip等のパッケージ管理サービスはすぐ使える形で配布させてくれるが、一般への公開サービスであるためすぐに消すことができない
    * Dockerコンテナで配布すると、すぐ使える状態で配布できるだけでなく、Gitlabはすぐに公開を止めることができるので、グループ内での配布にぴったり！



## 何語！？で、結局なにができるようになったの？(怒

* 一言でいうと、Gitlabに原稿をcommit/pushすると自動で校正をかけてくれるツール/環境を構築した

* みんながインストールしなくても使えるから便利

* (画面のスクショを張る)

  

## どうやるのか

* 基本的には下記のプロセスでやっていく

  * Textlint実装(基本コードは書かない)

    0. (Node.js環境のインストール)
    1. textlintインストール
    2. 好きなルールのインストール
       (独自のルールを作りたいときは開発)
    3. 実行して遊ぶ

  * DockerでTextlintをすぐに実行できる環境をコンテナに詰めて配布する(ここはちょっとコードを書く)

    0. (Docker環境の構築)

    1. 利用したい言語の既成Dockerコンテナを探す(今回はNode.js)
    2. Dockerコンテナに「必要なライブラリをインストールしてくださいね、設定はこうしてくださいね」と指示するファイルを作る(=dockerfileの作成)
    3. 動かしてみる
    4. Gitlab CIの設定をする(.gitlab-ci.ymlの作成。ほぼコピペ)

  * コンテナを使ってGitlab CIで原稿自動チェックができる環境を整える

    1. Gitlab CIの設定をする



## 実際にやってみる

### Textlint実装

#### 0.(Node.js環境のインストール)

* Windows

  * https://nodejs.org/en/
  * 互換性は結構あるので最新のLTS(安定版)でよいです

* Mac/Linux

  ```bash
  brew/apt/yum install node
  ```



#### 1.textlintインストール

* 以上、終了。

  ```bash
  npm install -g textlint
  ```

※Node.jsは、「あるフォルダ内で使うライブラリ」(ローカルインストール)と「どのフォルダでも使うライブラリ」(グローバルインストール)が選択制。今回はグローバルインストールを行うため -gが必要。



#### 2.好きなルールのインストール

* 例えば、ソフトウェア開発者向け雑誌「WEB DB PRESS」誌の構成ルールをインストールしてみよう

```bash
npm install -g textlint-rule-web-plus-db
```



#### 3.実行して遊ぶ

* 好きなmarkdownファイルに対して実行してみよう

  ```bash
  textlint --rule web-plus-db(ルールの「textlint-rule-」以下) text.md(markdownファイル)
  ```

  * デフォルトだとtxtファイルとmdファイルを対象にできるが、ほかのファイルを使いたい場合はプラグインがある。

* 使いたいルールが複数ある場合は「.textlintrc」というファイルを作って、JSON記法で以下のように記載

  ```json
  {
      "rules":{
          "rule1(ルール名)":true,
          "rule2(ルール名)":true
      }
  }
  ```

* ものによっては外部ファイルを参照するものがあります。

  * 外部の校正規則ファイルを読み込んで表記を統一する[Prh](https://github.com/textlint-rule/textlint-rule-prh)

    ```json
    {
        "rules": {
            "prh": {
                "rulePaths" :["./prh-rules/rule1.yml", "./prh-rules/rule2.yml"]
            }
        }
    }
    ```

  * 大体githubリポジトリのReadme.mdに「.textlintrc」ルールの書き方が記載されています。



#### 4.(上級編)好きなルールを開発する

* 今回、書籍作成に向けて行いたかったのは、

  ```txt
  * 今回採用するボードゲーム「Wingspan」はカード270枚を収録しており、極めて類似するものを含め英語名にて270種類の鳥名が記載される
  * 個人の自助努力でも可能だが、可能なら仕組みで解決したい
  ```

  という仕組みを自動校正でもやろうとしていた。

* ただし、該当するルールは見つからず→そうだ、自分で作ろう！

  * 作ったもの

    * 自作の用語リストを読み込み、それに表記を統一されているかエラーを出す
    * 文章の中のシングルバイト文字(≒半角文字)を抜き出して、リスト内の表記と合致するものがあるかを探索する
    * なければエラーとして「この単語はリスト内にありません」と返す

  * ルールの作り方
    必要なファイルを生成するツールが既にあり、その中のJavascript「index.js」を修正するだけ
    https://efcl.info/2016/12/14/create-textlint-rule/

  * 詳細はこの場では説明しないけど、コードの記述量はこんなもん

    ```javascript
    "use strict";
    //必要なライブラリのインストール
    //文字列からフォルダ位置を生成するライブラリ
    const path = require("path");
    //相対参照を絶対参照にするライブラリ
    const untildify = require("untildify");
    //ファイル読み込み/書き出しを行うライブラリ
    const fs = require('fs')
    
    //外部のテキストファイルから単語リストを読み込むメソッド
    function createBirdList(rulePaths,baseDir) {
    	//テキストファイルのパス(ディレクトリ)は配列で入力させる=複数読み込み可能
        //もし指定されていなかったら処理を終了する
        if (rulePaths.length === 0) {
            return null;
        }
        //テキストファイルのパスを相対参照から絶対参照にする
        const expandedRulePaths = rulePaths.map(rulePath => untildify(rulePath));
        //テキストファイルを読み込む
        const text = fs.readFileSync(path.resolve(baseDir, expandedRulePaths[0]))
    	//「改行記号」で分割して配列として取り込む(改行記号はWin/Macで異なるので後悔ポイント)
        const birdList = text.toString().split('\n');
    	//もしルールが複数あるならそれらも読み込んで行列に統合する
        expandedRulePaths.slice(1).forEach(ruleFilePath => {
            const addtext = readFileSync(path.resolve(baseDir, expandedRulePaths[0]));
            const config = addtext.toString().split('\n')
            birdList.concat(config);
        });
        //単語リスト配列を返り値として返す
        return birdList;    
    }
    
    //文内からシングルバイト文字を抜き出すメソッド。テキストは一行ずつ読み込まれる想定
    function pickUpSingleBytePhraseDetail(text){
    	//シングルバイト文字を抜き出す正規表現(Qiitaより拝借)。前後が【】で囲まれててもOKとした
        const ex = /【*[a-zA-Z0-9!-/:-@¥[-`{-~\s][a-zA-Z0-9!-/:-@¥[-`{-~\s]+】*/g
    	//空の配列を生成
        let array =[]
    	//空の変数を生成(Javascriptはあらかじめ変数の宣言が必要)
        let arr;
    	//text(一行ずつ読み込まれた文章)に対して、該当するものがなくなるまで、文末まで上記の正規表現「ex」のルールに従って探索(コマンドの都合上、1回の探索で1つしか抽出できないため)
        while((arr = ex.exec(text)) != null){
    	//探索出来たら[(該当する単語),(該当する単語が文頭から何文字目か)]を配列に格納
            array.push([arr[0],arr.index])
        }
        //文章に対して最後まで探索を実行したら配列としてエラーリストを返す
        return array
    }
    //「textlintrc」というフォルダからデータを読み込むための部分。Prhより拝借
    const getConfigBaseDir = context => {
        if (typeof context.getConfigBaseDir === "function") {
            return context.getConfigBaseDir() || process.cwd();
        }
        // Old fallback that use deprecated `config` value
        // https://github.com/textlint/textlint/issues/294
        const textlintRcFilePath = context.config ? context.config.configFile : null;
        // .textlintrc directory
        return textlintRcFilePath ? path.dirname(textlintRcFilePath) : process.cwd();
    };
    //ここからは実行ファイル。外部のファイルから関数として利用するため、module.exportsから書き出す
    module.exports = function (context, options = {}) {
    	//contextとして文章データを格納。この辺は丸々コピペ
        const { Syntax, RuleError, report, getSource } = context;
        //textlintrcファイルから、必要な情報を抜き出す
        const textlintRCDir = getConfigBaseDir(context);
    	//textlintrcファイルのJSONからファイルの置き場所(rulePaths)を読み込む
        const rulePaths = options.rulePaths || [];
        //チェックする単語リストを読み込む(自作関数)
        const birdList = createBirdList(rulePaths, textlintRCDir)
        //エラー結果を返す
        return {
    		//一文ずつ処理する。
            [Syntax.Str](node) { // "Str" node
    			//各行ごとに行数等の情報を持ったJSONとして読み込まれているので、その中から文面だけを抜き出す
                const text = getSource(node); // Get text
    			//抜き出した文章に対してシングルバイト文字列単語を配列リストとして抜き出す(自作関数)
                const singleByteWords = pickUpSingleBytePhraseDetail(text)
    			//もし該当する文字列がなければ処理終了
                if (!singleByteWords) {
                    return
                }
    			//各行のシングルバイト単語ごとに単語リストを参照して該当するものがあるかを☑
                singleByteWords.map(element => {
                    //もし該当するものがあればスルー
                    if (birdList.indexOf(element[0]) != -1) {
                        return
                    }
                    //該当するものがなければリストにありませんでした、というエラーを出す
                    const ruleError = new RuleError(element[0]+"はリストにありませんでした",{index: element[1]})
                    //エラーとして警告を作成する
                    report(node, ruleError)
                })
            }
        }
    };
    ```

* これをビルド`npm run build`したらWeb上のルールと同様に使えるライブラリになる



### DockerでTextlintをすぐに実行できる環境をコンテナに詰めて配布する

* 背景
  * 本当はこんなことはしなくてもよいので、ここからは自己満足(えっ)
  * 自作のルールを使い、なおかつ煩雑なインストール作業をさせないため
  * DockerファイルとしてGitlabCIに読み込ませればコードやコマンド実行させなくても配布できる！

#### 0.Docker環境の構築

* 方法としては、ボタン押してインストールするだけ
  * https://www.docker.com/get-started
* 注意
  * ここからはWindows10 Homeの人は該当しないよ、ごめんね
    (Win10 Pro、MaxOSX、LinuxだったらOK)
  * WindowsでやるときはHyper-Vという機能が必要で、それはProにしか入ってない
  * Win10 Homeの人はAWS上でやるのがよさそう

#### 1.利用したい言語の既成Dockerコンテナを探す

* node.js、Python、Anaconda…と各言語に応じた「言語インストール済みコンテナ」が公式で落ちているので、それを使う
  * 「(言語名) DockerHub」と検索したら大体出ます
  * 基本的には最新バージョンの軽量版コンテナを使っておくのが良いのではないかと思われる

#### 2.Dockerコンテナに環境設定を指示するファイルを作る

* 「Dockerfile」というファイルを作る。まずはご覧ください

  ```dockerfile
  # FROMはベースにするコンテナの指示
  # 「node」(nodejs)コンテナの「lts-alpine」をつかう、という意味
  # (ltsは安定版最新バージョン、OSは軽量型Linuxのalpine)
  FROM node:lts-alpine
  
  # RUNはコンテナ内でOSに対して実行するコマンドを指示
  # node.jsのライブラリインストールを行う。インストールするのは「textlint」とそのルール
  RUN npm i -g \
      textlint \
      textlint-plugin-review \
      textlint-rule-ja-yahoo-kousei
  
  # COPYはコンテナ外部のファイルをコンテナ内部にコピーしてくる
  # textlint-rule-torint(自作ルール)をコンテナ内部にコピー
  COPY textlint-rule-torint /textlint-rule-torint
  
  # 自作ルールをインストール
  RUN npm i -g /textlint-rule-torint
  
  ```

* 乱暴に言えば、「FROM」「RUN」「COPY」さえ覚えておけば大抵のことはできる

  * 特にRUNがそのまま実行コマンド書けばいいのは楽ちん

* 他もあと10コマンドくらいしかない

  * http://docs.docker.jp/engine/reference/builder.html

#### 3.動かしてみる

* ローカルのDocker環境で動かしてみる

* Dockerfileがあるディレクトリ内で下記を実行すると、Dockerfileに応じてコンテナが作られる

  ```sh
  docker build (作者名)/(コンテナ名):(バージョン。通常は1.0) .
  ```

  * 例

    ```bash	
    docker build Omochicard/test:1.0 .
    ```

* コンテナができたら動かしてみる

  * 今度は場所を問わない

    ```bash
    docker run (作者名)/(コンテナ名)
    ```

  * 今回はtextlintなので、

    ```bash
    docker run Omochicard/test:1.0 Readme.md
    ```

    等々。

#### 4.Gitlabの配布スペースに配置できるようにGitlab CIの設定をする

* 動作が確認出来たら、次は配布できるようにGitlab CIの設定をする

* 汎用化されているので、これをコピペして「.gitlab-ci.yml」として「dockerfile」と一緒にディレクトリ直下に配置

  * ```yml
    image: docker:latest
    services:
      - docker:dind
    
    variables:
      IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    
    before_script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    
    build:
      script:
        - docker build -t $IMAGE_TAG .
        - docker push $IMAGE_TAG
    ```

* 出来たらPush。
* GitlabCI内の「Packages」→「Container Registry」にコンテナができているか確認。できていたら下記のようになっている
  ![image-20200403092135553](/uploads/d9785c907a52499f4c5fe36a2ff28103/image-20200403092135553.png)

* 出来たら動作確認。Container Registry内のコンテナ名を選択すると
  ![コンテナレジストリ_tag](/uploads/b7cef4755b9e365c9018042d9d3b9f00/image-20200403092304106.png)
  となっているので、コピーマークからコピー。

  * 下記コマンドを打ち込んで動作すれば動作確認完了

    ```bash
    docker run (コピペ内容。「registry.gitlab.com/～」となっている)
    ```

### コンテナを使ってGitlab CIで原稿自動チェックができる環境を整える

#### 1.Gitlab CIの設定をする

* ここはHands Onと同様。コンテナのimageは自作した「registry.gitlab.com/～」にしておく