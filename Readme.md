# ボドゲ攻略本を支える技術



## 全体の背景

* 毎年春秋に開催される、国内最大のボードゲーム関連即売会/新作発表会の「ゲームマーケット」において攻略本書籍を発表する予定
* CSS組版をはじめとしたWeb組版基盤を使っての入稿原稿作成等、さまざまな技術利用を行っており、そのうちいくつかを紹介



## なぜ学ぶのか

* 書籍執筆をはじめとしたドキュメンテーション作業を効率化したい
* 書籍には興味がないけどDevops的なものの入門にしたい
* VScode拡張の作り方を知りたい
* CI/CD、スニペット、CSS組版、という言葉を使いたい



## 今回紹介する技術

* [表記統一のためのVSCodeスニペット利用](https://gitlab.com/Morimr/tech_for_boardgamebook/-/tree/master/VSCode_Snippet)
* [Textlint自動校正の導入(記者ハンドブックベース校正を自動化しよう)](https://gitlab.com/Morimr/tech_for_boardgamebook/-/tree/master/Hands_on)
* [Textlint自動校正のカスタマイズ(上級編)](https://gitlab.com/Morimr/tech_for_boardgamebook/-/tree/master/Textlint_Customize)
