# 表記統一のためのVSCodeスニペット

## 背景

* 複数人で原稿を執筆し、表記統一はただでさえ困難
* 今回採用するボードゲーム「Wingspan」はカード270枚を収録しており、極めて類似するものを含め英語名にて270種類の鳥名が記載される
* 個人の自助努力でも可能だが、可能なら仕組みで解決したい
* IMEツールを使った変換では全角入力せねばならず、辞書ツールでの解決も難しい
* Re:viewというファイルフォーマットを使うため、個々人の執筆環境はVSCode



## 技術の概要

* VSCodeのスニペット作成機能をつかって簡単に入力予測環境を実装
* 大量の項目があったが、リスト形式のファイルを正規表現で一発変換し、短時間で作成
* 簡単にインストールできるファイルを作成する「パッケージ作成機能」を使って、技術に疎い人含めてノンコードでの表記環境統一を実現



## どうやるのか

* 基本的には下記の3ステップで作る
  * A.スニペットを定義するJSONファイルを作る(+これだけでも可能なのでその方法)
  * B. VSCode拡張プロジェクトを作る(+スニペットを適用する条件を指定する)
  * C. VSCode拡張パッケージファイルを作る(GUIインストール用/配布用のファイルを作る)





### A. スニペットを定義するJSONファイルを作る

* まずは作成するコードをご覧いただきたい

```Json	
	{
        "(スニペット名)":{
          "prefix": "(どういう文字を打った時に入力補助したいか)",
           // ちなみに部分一致は入力補助されます。"J","Ja","Javascript"のようにしなくてOK
          "body": ["補完したい内容。複数行にわたる場合は「,」で配列として入力する"],
          "description":"(任意)内容を説明したいとき"
        },
        // 例
        "Acorn Woodpecker": {
		"prefix": "Acorn Woodpecker",
		"body": ["【Acorn Woodpecker】"]
		}
        // 「Acorn Woodpecker」と入力したとき/する過程で「【Acorn Woodpecker】」と補完したかった
    }
```

* 基本的にはこれだけ

※JSONファイルだけでスニペット登録する方法

(画像)



* ただしこれだけでは使い勝手が悪い
  * 登録するのにいちいちファイルの書き換えが必要&コマンドが細かい
    * 間違えたら怖い
    * 人に配ったときに使ってもらえなさそう(特にITに疎い人)
  * 同じファイル形式で別のものを作りたいとき、前に作ったスニペットの入力補完が残ってると鬱陶しい
    * GUIで切り替えたい

**→VSCodeの拡張機能ファイルを作ってしまえば、GUIでインストール/アンインストールできる！**



### B. VSCode拡張プロジェクトを作る(+スニペットを適用する条件を指定する)

* 必要なもの
  * Node.jsの開発環境**(※今回Javascript、Node.jsは1行も書きません)**
    https://qiita.com/Masayuki-M/items/840a997a824e18f576d8



0. 拡張プロジェクトを作るのに必要なパッケージをインストールする

   ```shell
   npm install yo generator-code -g (npmはnode.js版pipみたいなもの)
   ```



1. VS Code拡張プロジェクトを作る

   * Shellに以下のコマンドを入力して、質問に答えるだけで自動で生成してくれます。

   ``` shell
   $yo code
   # ? What type of extension do you want to create?
   New Code Snippet
   # ? What's the name of your extension?
   (スニペットの名前)
   ### Press <Enter> to choose default for all options below ###
   
   # ? What's the identifier of your extension?
   (任意。スニペットの識別子なので特に問題なければ空白EnterでOK)
   # ? What's the description of your extension?
   (任意。このスニペットの説明文)
   # ? Initialize a git repository?
   yes(空白EnterでOK)
   # ? Which package manager to use?
   npm(空白EnterでOK)
   ```

   以下のようなディレクトリ(フォルダ)ができたら完成。
   (画像)



### C. VSCode拡張パッケージファイルを作る(GUIインストール用/配布用のファイルを作る)

具体的には以下を参考にしました

https://qiita.com/sho7650/items/73006adbeb65156cf0cc

1. 「A.スニペットを定義するJSONファイルを作る」で作ったJSONを格納する

   * 作ったディレクトリの中の「Snippet」フォルダ内の「Snippets.json」ファイルにコピペするだけ

2. Azure Devopsアカウントを作る**(無料)**

3. Azure Devopsアカウントを登録する**(無料)**

   * (画像で説明)

4. Readme.mdを書き換える

   * デフォルトのままだと怒られるので、全部消して「ああああ」でもOK。
   * 具体的にはここで表示される説明文になります
     (画像)

5. パッケージファイルを作る

   * 以下のコマンドを打つだけ

     ```shell
     vsce package
     ```
     
   * 「XXXX.vsix」ファイルができたら完成

## インストールの方法

* パッケージインストール画面から「VSIXファイルからインストール」を押してインストール
  (画像)
* プロジェクト限定適用のやり方
  * (調べて書く)